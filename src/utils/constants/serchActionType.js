export class SearchActionType {
  static FETCH_POPSEARCH_REQUEST = 'FETCH_POPSEARCH_REQUEST';
  static FETCH_POPSEARCH_SUCCESS = 'FETCH_POPSEARCH_SUCCESS';
  static FETCH_POPSEARCH_FAILED = 'FETCH_POPSEARCH_FAILED';

  static FETCH_POPTRAINER_REQUEST = 'FETCH_POPTRAINER_REQUEST';
  static FETCH_POPTRAINER_SUCCESS = 'FETCH_POPTRAINER_SUCCESS';
  static FETCH_POPTRAINER_FAILED = 'FETCH_POPTRAINER_FAILED';

  static FETCH_CATEGORIES_REQUEST = 'FETCH_CATEGORIES_REQUEST';
  static FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
  static FETCH_CATEGORIES_FAILED = 'FETCH_CATEGORIES_FAILED';

  static FETCH_SEARCHRESULT_REQUEST = 'FETCH_SEARCHRESULT_REQUEST';
  static FETCH_SEARCHRESULT_SUCCESS = 'FETCH_SEARCHRESULT_SUCCESS';
  static FETCH_SEARCHRESULT_FAILED = 'FETCH_SEARCHRESULT_FAILED';
}
