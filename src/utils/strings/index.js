const appString = {
  button: {
    goback: 'Go back',
  },
  pages: {
    welcome: {
      title: 'Get The Freshest Fruit Salad Combo',
      subtitle:
        'We deliver the best and freshest fruit salad in town. Order for a combo today!!!',
      button: {
        next: 'Let’s Continue',
      },
    },
    authentication: {
      title: 'What is your firstname?',
      placeholder: 'Input your first name',
      button: {
        startOrder: 'Start Ordering',
      },
    },
    home: {
      title: 'Hello Tony, What fruit salad combo do you want today?',
      placeholderSearch: 'Search for fruit salad combos',
      basketTitle: 'My basket',
      recommend: 'Recommended Combo',
    },
    menuDetail: {
      button: {
        add: 'Add to basket',
      },
    },
    orderList: {
      title: 'My Basket',
      total: 'Total',
      button: {
        checkout: 'Checkout',
      },
    },
    completeDetails: {
      address: 'Delivery address',
      number: 'Number we can call',
    },
    orderComplete: {
      title: 'Congratulations!!!',
      subtitle: 'Your order have been taken and is being attended to',
      button: {
        track: 'Track order',
        next: 'Continue shopping',
      },
    },
    trackOrder: {
      title: 'Delivery Status',
      firstStep: 'Order Taken',
      secondStep: 'Order Is Being Prepared',
      thirdStep: {
        title: 'Order Is Being Delivered',
        subtitle: 'Your delivery agent is coming',
      },
      lastStep: 'Order Received',
    },
    inputCardDetails: {
      name: 'Card Holders Name',
      number: 'Card Number',
      date: 'Date',
      ccv: 'CCV',
      button: {
        complete: 'Complete Order',
      },
    },
  },
};

export default appString;
