import ApiRequest, { apiInstance } from './config';
import baseUrl from './url';

const API = {};

//Example
API.getPhotos = ApiRequest.get(baseUrl.jsonPlaceholder.photos);

// Search
API.searchCourse = ApiRequest.get(baseUrl.search.searchResult);
API.popularTrainer = ApiRequest.get(baseUrl.search.popularTrainer);

// Wishlist
API.getMyWishlist = ApiRequest.get(baseUrl.wishlist.myWhislist);

// My Course
API.getCourseCategories = ApiRequest.get(baseUrl.courses.categoriess);
API.createCourse = ApiRequest.post(baseUrl.myCourse.postCourse);
API.updateCourse = ApiRequest.post(baseUrl.myCourse.updateCourse);
API.getMyCourse = ApiRequest.get(baseUrl.myCourse.getCourse);
API.postUpdatePhoto = ApiRequest.post(baseUrl.myCourse.postImage);
API.postMateri = ApiRequest.post(baseUrl.myCourse.postMateri);
API.getMateri = ApiRequest.get(baseUrl.myCourse.getMateri);
API.updateMateri = ApiRequest.get(baseUrl.myCourse.updateMateri);
API.updateFileMateri = ApiRequest.get(baseUrl.myCourse.updateFileMateri);
API.getAuthorExam = ApiRequest.get(baseUrl.myCourse.getExam);
API.postExam = ApiRequest.post(baseUrl.myCourse.postExam);
API.updateExam = ApiRequest.post(baseUrl.myCourse.updateExam);
API.publishCourse = ApiRequest.post(baseUrl.myCourse.publishCourse);
API.unpublishCourse = ApiRequest.post(baseUrl.myCourse.unpublishCourse);

//Course
API.getCategory = ApiRequest.get(baseUrl.course.category);
API.getNewCourse = ApiRequest.get(baseUrl.course.newCourse);
API.getRecent = ApiRequest.get(baseUrl.course.recent);
API.getMostLike = ApiRequest.get(baseUrl.course.mostLike);
API.getPopular = ApiRequest.get(baseUrl.course.popular);
API.getUserPreference = ApiRequest.get(baseUrl.course.userPreference);
API.getCourseByCategory = ApiRequest.get(baseUrl.course.courseByCategory);
API.toggleWishlist = ApiRequest.post(baseUrl.course.toggleWishlist);
API.getSimilarCourse = ApiRequest.get(baseUrl.course.similar);
API.getCommentCourse = ApiRequest.get(baseUrl.course.commentList);
API.toggleLove = ApiRequest.post(baseUrl.course.toggleLove);
API.takeCourse = ApiRequest.post(baseUrl.course.takeCourse);
API.getCourseByCategory = ApiRequest.get(baseUrl.course.courseByCategory);
API.postUserPreference = ApiRequest.post(baseUrl.course.userPreference);
API.countView = ApiRequest.post(baseUrl.course.countView);
API.checkCourseStatus = ApiRequest.post(baseUrl.course.checkCourseStatus);
API.readMaterial = ApiRequest.get(baseUrl.course.readMaterial);
API.flagCompleteMaterial = ApiRequest.post(baseUrl.course.flagCompleteMaterial);
API.submitReview = ApiRequest.post(baseUrl.course.submitReview);
API.getExam = ApiRequest.get(baseUrl.course.getExam);
API.takeExam = ApiRequest.post(baseUrl.course.takeExam);

API.getSubscribedCourseList = ApiRequest.get(
  baseUrl.myCourse.getSubscribedCourseList,
);
API.getCourseScore = ApiRequest.get(baseUrl.myCourse.getScore);

//Auth
API.Login = ApiRequest.post(baseUrl.auth.login);
API.Register = ApiRequest.post(baseUrl.auth.register);

//Profile
API.getTrainerProfile = ApiRequest.get(baseUrl.profile.trainer);

//onboarding
API.getOnboarding = ApiRequest.get(baseUrl.onboarding);

//API Profile
API.getProfile = ApiRequest.get(baseUrl.profil.getprofile);
API.updateProfile = ApiRequest.put(baseUrl.profil.editprofile);
API.uploadProfilePicture = ApiRequest.post(baseUrl.profil.uploadprofilepicture);
API.sendEmail = ApiRequest.post(baseUrl.profil.sendemail);
API.verifyEmail = ApiRequest.post(baseUrl.profil.verifyemail);
API.getNotification = ApiRequest.get(baseUrl.profil.getNotification);
API.readNotification = ApiRequest.post(baseUrl.profil.readNotification);

//firebase
API.storeToken = ApiRequest.post(baseUrl.firebase.storeToken);
API.Register = ApiRequest.post(baseUrl.auth.register);

API.checkVersion = ApiRequest.post(baseUrl.app.version);

export default API;
