import axios from 'axios';
import _ from 'lodash';
import Env from 'react-native-config';
import { AsyncStorage } from 'libraries';

export const apiInstance = axios.create({
  baseURL: '',
  timeout: 45000,
  // validateStatus: (status) => status >= 200 && status < 300,
});

class ApiRequest {
  static get(route) {
    return (payload) => this.request('GET', route, payload);
  }

  static put(route) {
    return (payload) => this.request('PUT', route, payload);
  }

  static post(route) {
    return (payload) => this.request('POST', route, payload);
  }

  static delete(route) {
    return (payload) => this.request('DELETE', route, payload);
  }

  static setupHeader({ payload }) {
    let ContentType = 'application/json';
    if (payload.type === 'form-data') {
      ContentType = 'multipart/form-data';
    }
    let headers = {
      ['Timestamp']: `${Date.now()}`,
      ['Content-Type']: ContentType,
      ['AppKey']: `Apikey ${Env.API_KEY}`,
      ['Authorization']: `Bearer ${payload.token}`,
    };

    if (payload.headers) {
      headers = { ...headers, ...payload.headers };
    }
    return headers;
  }

  /**
   * handle url params, input object, return string
   * @param {object} params
   */
  static setupParams(params) {
    const paramsResult = [];
    Object.keys(params).map((e) => paramsResult.push(`${e}=${params[e]}`));
    return `?${paramsResult.join('&')}`;
  }

  /**
   * handle url params, input object, return string
   * @param {object} params
   */
  static setupParamsId(payload) {
    let params = '';
    if (payload.params) {
      params = this.setupParams(payload.params);
    }
    return `/${payload.paramsId}${params}`;
  }

  static setupUrl(payload) {
    const { path, route, params, customUrl } = payload;

    let endpointUrl = route + path + params;

    if (customUrl) {
      endpointUrl = customUrl + path + params;
    }

    return endpointUrl;
  }

  /**
   * handle API request
   * @param {string} method
   * @param {string} route
   * @param {object} payload
   */
  static async request(method, route, payload = {}) {
    let path = '';
    let params = '';

    payload.token = await AsyncStorage.getItem('@token');
    let headers = this.setupHeader({ payload });

    if (payload.params) {
      params = this.setupParams(payload.params);
    }

    if (payload.paramsId) {
      params = this.setupParamsId(payload);
    }

    let url = this.setupUrl({
      path,
      route,
      params,
    });

    const requestConfig = {
      url,
      method,
      headers,
    };

    if (payload.body && !_.isEmpty(payload.body)) {
      requestConfig.data = payload.body;
    }

    // console.log(
    //   `REQ-${requestConfig.method}-${requestConfig.url} \n`,
    //   requestConfig,
    // );

    try {
      const res = await apiInstance.request(requestConfig);
      const response = res.data;
      // console.log(
      //   `RESP-${requestConfig.method}-${requestConfig.url} \n`,
      //   response,
      // );
      return Promise.resolve(response);
    } catch (err) {
      const error = err.response ? err.response : err;

      // console.log(
      //   `RESPERR-${requestConfig.method}-${requestConfig.url} \n`,
      //   error,
      // );
      return Promise.reject(error);
    }
  }
}

export default ApiRequest;
