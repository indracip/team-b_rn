//Example
export const base_url = 'http://139.59.124.53:3006';
const base_url_placeholder = 'http://jsonplaceholder.typicode.com';
const base_url_training = 'http://139.59.124.53:3006';

const baseUrl = {
  course: {
    category: `${base_url}/api/course/category`,
    newCourse: `${base_url}/api/course/latest`,
    recent: `${base_url}/api/user/course/recent`,
    mostLike: `${base_url}/api/course/most-like`,
    popular: `${base_url}/api/course/most-popular`,
    userPreference: `${base_url}/api/user/preference`,
    courseByCategory: `${base_url}/api/course/course-by-category`,
    toggleWishlist: `${base_url}/api/user/wishlist/toggle`,
    similar: `${base_url}/api/course/similar`,
    commentList: `${base_url}/api/course/comments/get`,
    toggleLove: `${base_url}/api/course/love/toggle`,
    takeCourse: `${base_url}/api/user/subscribe/create`,
    countView: `${base_url}/api/user/course/view`,
    checkCourseStatus: `${base_url}/api/user/course/check`,
    readMaterial: `${base_url}/api/course/material`,
    flagCompleteMaterial: `${base_url}/api/user/material/complete`,
    submitReview: `${base_url}/api/user/course/review`,
    getExam: `${base_url}/api/course/exam`,
    takeExam: `${base_url}/api/user/exam/take`,
  },
  auth: {
    login: `${base_url}/api/user/login`,
    register: `${base_url}/api/user/signup`,
  },
  profile: {
    trainer: `${base_url}/api/author/detail`,
  },
  search: {
    searchResult: `${base_url}/api/course/search`,
    popularTrainer: `${base_url}/api/author/popular`,
  },
  wishlist: {
    myWhislist: `${base_url}/api/user/wishlist/list`,
  },
  courses: {
    categories: `${base_url}/api/course/category`,
  },
  myCourse: {
    getCourse: `${base_url}/api/author/course/list`,
    postCourse: `${base_url}/api/author/course/create`,
    getSubscribedCourseList: `${base_url}/api/user/subscribe/list`,
    getScore: `${base_url}/api/user/exam/result`,
    updateCourse: `${base_url}/api/author/course/update`,
    postImage: `${base_url}/api/author/course/image/upload`,
    getMateri: `${base_url}/api/author/course/material`,
    postMateri: `${base_url}/api/author/course/material/create`,
    updateMateri: `${base_url}/api/author/course/material/update`,
    updateFileMateri: `${base_url}/api/author/course/material/file/upload`,
    getExam: `${base_url}/api/author/course/exam`,
    postExam: `${base_url}/api/author/exam/create`,
    updateExam: `${base_url}/api/author/exam/update`,
    publishCourse: `${base_url}/api/author/course/publish`,
    unpublishCourse: `${base_url}/api/author/course/unpublish`,
  },
  jsonPlaceholder: {
    photos: `${base_url_placeholder}/photos`,
  },
  onboarding: `${base_url}/api/apps/illustration`,
  profil: {
    getprofile: `${base_url_training}/api/user/profile/get`,
    editprofile: `${base_url_training}/api/user/profile/update`,
    uploadprofilepicture: `${base_url_training}/api/user/profile/picture/upload`,
    getprofilepicture: `${base_url_training}/files/user/thumb/`,
    sendemail: `${base_url_training}/api/user/verification/send`,
    verifyemail: `${base_url_training}/api/user/verify`,
    getNotification: `${base_url_training}/api/user/getnotification`,
    readNotification: `${base_url_training}/api/user/readnotification`,
  },
  firebase: {
    storeToken: `${base_url}/api/user/subscribe/insertToken`,
  },
  app: {
    version: `${base_url}/api/apps/version/updates`,
  },
};

export default baseUrl;
