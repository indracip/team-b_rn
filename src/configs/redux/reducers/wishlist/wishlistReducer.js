import { WishlistActionType } from 'utils';

const defaultState = {
  wishlist_data: [],
  isLoading: false,
  errors: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case WishlistActionType.FETCH_WISHLIST_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case WishlistActionType.FETCH_WISHLIST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        wishlist_data: action.payload,
      };
    }
    case WishlistActionType.FETCH_WISHLIST_FAILED: {
      return {
        ...state,
        isLoading: false,
        wishlist_data: action.payload,
        errors: action.error,
      };
    }
    default:
      return state;
  }
};
