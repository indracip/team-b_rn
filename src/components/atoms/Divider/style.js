import { StyleSheet } from 'libraries';
import { scale } from 'utils';

export const styles = StyleSheet.create({
  divider: {
    marginBottom: scale(10),
  },
});
