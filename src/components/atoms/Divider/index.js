import { React, View } from 'libraries';
import { styles } from './style';

const Divider = () => {
  return <View style={styles.divider} />;
};

export default Divider;
