import { StyleSheet } from 'libraries';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  backButton: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(50),
    padding: scale(4),
  },
  icon: {
    paddingHorizontal: scale(10),
    opacity: 1,
  },
});

export default styles;
