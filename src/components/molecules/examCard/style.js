import { StyleSheet } from 'libraries';
import { Color, scale, shadow } from 'utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: scale(5),
    padding: scale(10),
    marginBottom: scale(10),
    ...shadow,
  },
  typeContainer: {
    flexDirection: 'row',
    backgroundColor: Color.primaryColorLighten,
    borderRadius: scale(100),
    padding: scale(5),
    flexShrink: 1,
    alignItems: 'center',
  },
  typeLabel: {
    fontSize: scale(14),
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'uppercase',
  },
  questionContainer: {
    marginVertical: scale(5),
    padding: scale(5),
    // backgroundColor: Color.gray,
  },
  optionContainer: {
    padding: scale(5),
    flexDirection: 'row',
    alignItems: 'center',
  },
  optionKey: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  answerContainer: {
    flexDirection: 'row',
    padding: scale(5),
  },
  answerLabel: {
    fontWeight: 'bold',
  },
  answer: {
    textTransform: 'uppercase',
  },
  circleNumber: {
    width: scale(30),
    aspectRatio: 1 / 1,
    backgroundColor: Color.primaryColor,
    borderRadius: scale(100),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(5),
  },
  numberLabel: {
    fontWeight: 'bold',
    color: 'white',
  },
});

export default styles;
