import { StyleSheet } from 'libraries';
import { scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: scale(10),
    marginHorizontal: scale(5),
    padding: scale(5),
    paddingVertical: scale(10),
    borderRadius: scale(10),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  categoryLabel: {
    fontSize: scale(14),
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default styles;
