import { React, View, Pressable } from 'libraries';
import { Text } from 'components';
import PropTypes from 'prop-types';
import styles from './style';
import { scale } from 'utils';
import { Icon } from 'react-native-eva-icons';

const CategoryCard = (props) => {
  const { iconName, categoryName, onPress } = props;
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Icon
            name={iconName}
            width={styles.icon.width}
            height={styles.icon.height}
            fill={'#2D8989'}
          />
          <Text medium style={styles.text}>
            {categoryName}
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

CategoryCard.propTypes = {
  iconName: PropTypes.string,
  categoryName: PropTypes.string,
  onPress: PropTypes.func,
};

CategoryCard.defaultProps = {
  iconName: 'question-mark-outline',
  categoryName: '',
  onPress: () => {},
};

export default React.memo(CategoryCard);
