import { StyleSheet } from 'libraries';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    padding: scale(5),
    paddingHorizontal: scale(10),
    backgroundColor: 'white',
    marginBottom: scale(5),
  },
  firstLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  materialType: {
    backgroundColor: 'green',
    borderRadius: scale(5),
    padding: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(5),
  },
  materialTypeLabel: {
    color: 'white',
    fontSize: scale(14),
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  materialTitle: {
    color: Color.primaryColor,
    fontSize: scale(16),
    fontWeight: 'bold',
  },
});

export default styles;
