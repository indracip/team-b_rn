import { Text } from 'components/atoms';
import { React } from 'libraries';
import { TabBar } from 'react-native-tab-view';
import { Color, scale } from 'utils';
import styles from './style';

const MyCourseTabBar = (props) => {
  return (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={styles.container}
      scrollEnabled={true}
      tabStyle={styles.tab}
      activeColor={Color.primaryColor}
      inactiveColor="gray"
      renderLabel={({ route, focused, color }) => {
        return (
          <Text
            style={[
              styles.label,
              {
                color: color,
                fontSize: focused ? scale(16) : scale(14),
                // height: focused ? scale(30) : scale(20),
                marginTop: focused ? scale(0) : scale(5),
              },
            ]}>
            {route.title}
          </Text>
        );
      }}
    />
  );
};

export default MyCourseTabBar;
