import { CardCourseVertical } from 'components/organisms';
import { base_url } from 'configs/api/url';
import { React, EvaIcon, Image, TouchableOpacity, View, Text } from 'libraries';
import styles from './style';

const WishlistCard = (props) => {
  const { add, item, index, navigation } = props;

  return add === undefined ? (
    <CardCourseVertical
      key={index}
      title={item.title}
      desc={item.desc}
      lovedCount={item.loveCount.toString()}
      viewCount={item.viewCount.toString()}
      imageUri={
        item.picture !== null
          ? `${base_url}/files/course/image/${item.picture}`
          : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
      }
      isLoved={item.isLoved}
      isWishList={item.isWishlisted}
      onPress={() => {
        navigation.navigate('CourseDetailPage', {
          data: {
            ...item,
            id: item.courseId,
            authorUsername: item.authorUserName,
          },
        });
      }}
    />
  ) : (
    <View style={styles.wishlistCardContainer}>
      <TouchableOpacity
        style={styles.wishlistCardAddMore}
        onPress={() => {
          navigation.navigate('SearchPage');
        }}>
        <EvaIcon name="plus-circle" width={50} height={50} fill="white" />
      </TouchableOpacity>
    </View>
  );
};

export default WishlistCard;
