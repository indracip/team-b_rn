import CardCourseVertical from './cardCourseVertical';
import CardCourseHorizontal from './cardCourseHorizontal';
import MyCourseTabView from './myCourseTabView';
import MySubscriptionTabView from './mySubscriptionTabView';
import CommentCard from './commentCard';
import SearchPopularTrainer from './searchPopularTrainer';
import SearchPopularSearch from './searchPopularSearch';
// import FormCreateExam from './formCreateExam';

export {
  CardCourseVertical,
  CardCourseHorizontal,
  CommentCard,
  MyCourseTabView,
  MySubscriptionTabView,
  SearchPopularTrainer,
  SearchPopularSearch,
  // FormCreateExam,
};
