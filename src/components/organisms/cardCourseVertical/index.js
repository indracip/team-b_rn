/**
 * Card Course Vertical Component
 * @augments {CardCourseVertical<Props, State>}
 */ import { Text } from 'components/atoms';
import { Image, React, View, EvaIcon, Pressable } from 'libraries';
import PropTypes from 'prop-types';
import styles from './style';

const CardCourseVertical = (props) => {
  const {
    imageUri,
    title,
    desc,
    viewCount,
    lovedCount,
    isWishList,
    isLoved,
    onPress,
  } = props;
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>
        <Image
          source={{
            uri: imageUri,
          }}
          style={styles.image}
        />
        <Text numberOfLines={1} medium style={styles.title}>
          {title}
        </Text>
        <Text numberOfLines={2} style={styles.desc}>
          {desc}
        </Text>
        <View style={styles.footer}>
          <View style={styles.footerWrapper}>
            <View style={styles.footerViewLoved}>
              <EvaIcon
                name="eye-outline"
                width={styles.icon.width}
                height={styles.icon.height}
                fill={styles.icon.color}
              />
              <Text medium style={styles.footerText}>
                {viewCount}
              </Text>
              <EvaIcon
                name={isLoved ? 'heart' : 'heart-outline'}
                width={styles.icon.width}
                height={styles.icon.height}
                fill={styles.heart.color}
              />
              <Text medium style={styles.footerText}>
                {lovedCount}
              </Text>
            </View>

            <View style={styles.footerBookmark}>
              <EvaIcon
                name={isWishList ? 'bookmark' : 'bookmark-outline'}
                width={styles.icon.width}
                height={styles.icon.height}
                fill={isWishList ? 'orange' : styles.icon.color}
              />
            </View>
          </View>
        </View>
      </View>
    </Pressable>
  );
};

CardCourseVertical.propTypes = {
  imageUri: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  viewCount: PropTypes.string,
  lovedCount: PropTypes.string,
  isWishList: PropTypes.bool,
  isLoved: PropTypes.bool,
  onPress: PropTypes.func,
};

CardCourseVertical.defaultProps = {
  imageUri: '',
  title: '',
  desc: '',
  viewCount: '',
  lovedCount: '',
  isWishList: false,
  isLoved: false,
  onPress: () => {},
};

export default React.memo(CardCourseVertical);
