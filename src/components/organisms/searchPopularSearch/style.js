import { StyleSheet } from 'libraries';
import { Color, scale, verticalScale } from 'utils';

const styles = StyleSheet.create({
  container: { flex: 1, flexDirection: 'row', flexWrap: 'wrap' },
  searchBubble: {
    marginRight: scale(5),
    marginBottom: scale(5),
    paddingHorizontal: scale(15),
    paddingVertical: scale(5),
    borderRadius: verticalScale(100),
    backgroundColor: Color.primaryColor,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  topicName: { color: 'white' },
});

export default styles;
