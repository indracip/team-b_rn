import { Text } from 'components/atoms';
import { React, View, TouchableOpacity } from 'libraries';
import styles from './style';

const SearchPopularSearch = ({ popularSearch = [], ...props }) => {
  return (
    <View style={styles.container}>
      {popularSearch &&
        popularSearch.map((item) => {
          const { topicId, topicName } = item;
          return (
            <TouchableOpacity key={topicId} style={styles.searchBubble}>
              <Text style={styles.topicName}>{topicName}</Text>
            </TouchableOpacity>
          );
        })}
    </View>
  );
};

export default SearchPopularSearch;
