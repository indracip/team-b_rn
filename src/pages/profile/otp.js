import {
  React,
  useEffect,
  useState,
  connect,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'libraries';
import { Text, Button } from 'components';
import styles from './style';
// import { scale } from 'utils';
import LottieView from 'lottie-react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { sendEmail, sendOTP } from 'configs/redux/reducers/auth/authActions';
import Toast from 'react-native-toast-message';

const VerificationPage = (props) => {
  const { dispatchSendEmail, dispatchSendOTP, isLoading } = props;

  useEffect(() => {
    setTimeout(() => {
      if (timer > 0) {
        setTimer(timer - 1);
      }
    }, 1000);
    // console.log(otp.code);
  });

  useEffect(() => {}, []);

  const [timer, setTimer] = useState(59);
  const [otp, setOtp] = useState(0);
  const [isHide, setIsHide] = useState(false);

  function renderTimer() {
    if (timer === 0) {
      return (
        <Button
          onPress={() => {
            setTimer(59);
            setIsHide(true);
            dispatchSendEmail();
          }}
          buttonStyle={styles.otp1}
          textStyle={styles.otp2}
          title={'SEND LINK'}
        />
      );
    } else {
      return <Text style={styles.otp14}>{timer}</Text>;
    }
  }

  const doVerify = () => {
    if (otp === 0) {
      Toast.show({
        text1: 'Wrong code OTP',
        position: 'bottom',
        type: 'error',
      });
    } else {
      const payload = {
        body: {
          otp: otp,
        },
      };
      console.log(payload);
      dispatchSendOTP(payload);
    }
  };
  console.log(timer);
  return (
    // <KeyboardAvoidingView
    //   // keyboardVerticalOffset={10}
    //   behavior="padding"
    //   // style={styles.otp3}
    // >
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        {/* <View style={styles.otp4} /> */}

        <View style={styles.otp5}>
          <LottieView
            style={styles.otp6}
            source={require('../../assets/animasi/otp.json')}
            autoPlay
            loop
          />
          <Text style={styles.otp7}>Email Verification</Text>
          <Text style={styles.otp8}>
            Check your email to get link verification and submit OTP code
          </Text>
          {renderTimer()}
        </View>
        {timer > 0 ? (
          <View style={{ flex: 1 }}>
            <View style={styles.otp9}>
              <OTPInputView
                style={styles.otp10}
                pinCount={6}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged={(code) => {
                  setOtp(code);
                }}
                // autoFocusOnLoad
                editable={true}
                codeInputFieldStyle={styles.underlineStyleBase}
                // codeInputHighlightStyle={styles.underlineStyleHighLighted}
                // onCodeFilled={(code) => {
                //   setOtp(code);
                // }}
              />
            </View>

            <View style={styles.otp11}>
              <Button
                buttonStyle={styles.otp12}
                textStyle={styles.otp13}
                title={'SUBMIT'}
                onPress={() => doVerify()}
                isLoading={isLoading}
              />
            </View>
          </View>
        ) : (
          <View />
        )}
        <Toast ref={(ref) => Toast.setRef(ref)} />
      </View>
    </TouchableWithoutFeedback>
    // </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    isLoading: state.authStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchSendEmail: () => dispatch(sendEmail()),
    dispatchSendOTP: (payload) => dispatch(sendOTP(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VerificationPage);
