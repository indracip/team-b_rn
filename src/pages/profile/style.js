import { StatusBar } from 'react-native';
import { StyleSheet } from 'react-native';
import { Color, METRICS, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },

  borderStyleBase: {
    width: 30,
    height: 45,
    borderColor: 'black',
  },

  borderStyleHighLighted: {
    borderColor: '#2D8989',
  },

  underlineStyleBase: {
    width: 30,
    // height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'grey',
    fontSize: scale(20),
    fontWeight: 'bold',
    borderColor: '#2D8989',
  },

  underlineStyleHighLighted: {
    borderColor: '#2D8989',
  },

  style1: {
    backgroundColor: 'orange',
    marginLeft: scale(10),
    borderRadius: scale(20),
    paddingLeft: scale(5),
    paddingRight: scale(5),
  },

  style2: {
    color: 'white',
    fontSize: scale(12),
    fontWeight: 'bold',
    paddingVertical: scale(5),
    paddingHorizontal: scale(8),
  },

  style3: {
    flex: 0.5,
    justifyContent: 'flex-end',
    alignItems: 'center',
    //   backgroundColor: 'green',
    flexDirection: 'row',
    backgroundColor: '#2D8989',
  },

  style4: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '100%',
  },

  style5: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: '100%',
  },

  style6: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  style7: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },

  style8: {
    alignSelf: 'flex-start',
    marginRight: scale(20),
  },

  style9: {
    flex: 1.5,
    flexDirection: 'row',
    //   backgroundColor: 'transparent',
    borderRadius: scale(30),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 2.22,
    elevation: 3,
  },

  style10: {
    flex: 1,
    flexDirection: 'row',
    borderBottomLeftRadius: scale(30),
    borderBottomRightRadius: scale(30),
    backgroundColor: '#2D8989',
  },

  style11: {
    flex: 2.5,
    //   backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'center',
  },

  style12: {
    width: scale(120),
    height: scale(120),
    borderRadius: scale(120),
  },
  style13: {
    flex: 3.5,
    flexDirection: 'column',
    //   backgroundColor: 'green',
  },

  style14: {
    flex: 3,
    // backgroundColor: 'red',
    justifyContent: 'flex-end',
  },

  style15: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  style16: {
    flexDirection: 'row',
  },

  style17: {
    color: 'white',
    fontSize: scale(15),
  },

  style18: {
    flex: 3,
  },

  style19: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    // marginTop: scale(10),
  },

  style20: {
    alignSelf: 'flex-start',
    marginRight: scale(10),
  },

  style21: {
    fontSize: scale(15),
    fontWeight: 'bold',
    color: 'white',
  },

  style22: {
    flex: 3,
    backgroundColor: 'transparent',
    marginTop: scale(20),
    marginLeft: scale(20),
    marginRight: scale(20),
  },

  style23: {
    // flex: 1,
    justifyContent: 'center',
    backgroundColor: '#2D8989',
    borderRadius: scale(20),
    paddingVertical: scale(5),
  },

  style24: {
    fontWeight: 'bold',
    fontSize: scale(25),
    color: 'white',
    marginLeft: scale(20),
  },

  style25: {
    flex: 5,
    flexDirection: 'column',
    // backgroundColor: 'blue',
  },

  style26: {
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: '100%',
    // backgroundColor: 'yellow',
  },

  style27: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'blue',
  },

  style28: {
    width: scale(40),
    height: scale(40),
    borderRadius: scale(10),
  },

  style29: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: '100%',
    // backgroundColor: 'white',
  },

  style30: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style31: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'black',
  },

  style32: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style33: {
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: '100%',
    // backgroundColor: 'red',
  },

  style34: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'white',
  },

  style35: {
    width: scale(70),
    height: scale(70),
    borderRadius: scale(10),
  },

  style36: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: '100%',
    // backgroundColor: 'black',
  },

  style37: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style38: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'blue',
  },
  style39: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style40: {
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: '100%',
    // backgroundColor: 'green',
  },

  style41: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'white',
  },

  style42: {
    width: scale(70),
    height: scale(70),
    borderRadius: scale(10),
  },

  style43: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: '100%',
    // backgroundColor: 'black',
  },

  style44: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style45: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'blue',
  },

  style46: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style47: {
    flex: 1.5,
    flexDirection: 'row',
    // justifyContent: 'flex-end',
    height: '100%',
    // backgroundColor: 'grey',
  },

  style48: {
    // flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    marginRight: scale(10),
    // backgroundColor: 'white',
  },

  style49: {
    width: scale(40),
    height: scale(40),
    borderRadius: scale(10),
  },

  style50: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: '100%',
    // backgroundColor: 'black',
  },

  style51: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style52: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // backgroundColor: 'blue',
  },

  style53: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  style54: {
    flex: 1,
    // backgroundColor: 'grey'
  },

  style55: {
    alignSelf: 'flex-start',
    marginRight: scale(20),
  },

  style56: {
    color: '#2D8989',
    fontWeight: 'bold',
  },

  avatar1: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#2D8989',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowRadius: 11,
    shadowOpacity: 0.4,
    elevation: 50,
    paddingTop: scale(50),
  },

  avatar2: {
    flex: 1,
    width: '100%',
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: scale(60),
    marginTop: scale(10),
  },

  avatar3: {
    flex: 5,
    width: '100%',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    borderTopLeftRadius: scale(50),
    borderTopRightRadius: scale(50),
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowRadius: 11,
    shadowOpacity: 0.4,
    elevation: 50,
    marginTop: scale(5),
  },

  avatar4: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  avatar5: {
    color: '#2D8989',
    fontSize: scale(35),
    fontWeight: 'bold',
  },

  avatar6: {
    width: scale(200),
    height: scale(200),
    borderRadius: scale(270),
    // marginLeft: scale(55),
    marginTop: scale(30),
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarProfile: {
    width: scale(100),
    height: scale(100),
    borderRadius: scale(270),
    // marginLeft: scale(55),
    // marginTop: scale(30),
    alignItems: 'center',
    justifyContent: 'center',
  },

  avatar7: {
    flex: 5,
    alignItems: 'center',
  },

  avatar8: {
    marginHorizontal: scale(70),
    alignItems: 'center',
    backgroundColor: '#2D8989',
    borderRadius: scale(50),
    marginTop: scale(50),
    width: METRICS.window.width * 0.7,
  },

  avatar9: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  avatar10: {
    color: '#2D8989',
    fontSize: scale(15),
    fontWeight: 'bold',
  },

  edit1: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    backgroundColor: Color.backgroundColor,
  },

  edit2: {
    flexDirection: 'row',
    marginHorizontal: scale(50),
    borderRadius: scale(50),
    marginTop: scale(15),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
    paddingVertical: scale(5),
  },

  edit3: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },

  edit4: {
    color: 'red',
    marginLeft: scale(70),
    marginRight: scale(50),
  },

  edit5: {
    flexDirection: 'row',
    marginHorizontal: scale(50),
    borderWidth: 2,
    borderRadius: scale(50),
    borderColor: '#2D8989',
    marginTop: scale(15),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
  },

  edit6: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },

  edit7: {
    color: 'red',
    marginLeft: scale(70),
    marginRight: scale(50),
  },

  edit8: {
    flexDirection: 'row',
    marginHorizontal: scale(50),
    borderWidth: 2,
    borderRadius: scale(50),
    borderColor: '#2D8989',
    marginTop: scale(15),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
  },

  edit9: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },

  edit10: {
    color: 'red',
    marginLeft: scale(70),
    marginRight: scale(50),
  },

  edit11: {
    flexDirection: 'row',
    marginHorizontal: scale(50),
    borderWidth: 2,
    borderRadius: scale(50),
    borderColor: '#2D8989',
    marginTop: scale(15),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
  },

  edit12: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },

  edit13: {
    color: 'red',
    marginLeft: scale(70),
    marginRight: scale(50),
  },

  edit14: {
    alignItems: 'center',
  },

  edit15: {
    borderRadius: scale(20),
    backgroundColor: '#2D8989',
    justifyContent: 'flex-end',
  },

  edit16: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(50),
  },

  edit17: {
    fontSize: scale(15),
    color: 'white',
    marginTop: scale(150),
  },

  edit18: {
    paddingHorizontal: scale(120),
    marginTop: scale(30),
    backgroundColor: '#2D8989',
    borderRadius: scale(50),
  },

  edit19: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  otp1: {
    paddingHorizontal: scale(40),
    backgroundColor: '#2D8989',
    borderRadius: scale(50),
    marginTop: scale(20),
  },

  otp2: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  otp3: {
    flex: 1,
  },

  otp4: {
    flex: 0.5,
  },

  otp5: {
    flex: 2,
    //   backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },

  otp6: {
    width: scale(200),
    height: scale(250),
  },

  otp7: {
    fontSize: scale(30),
    fontWeight: 'bold',
    color: '#2D8989',
  },

  otp8: {
    fontSize: scale(12),
    color: '#2D8989',
  },

  otp9: {
    flex: 1,
    //   backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
  },

  otp10: {
    width: '80%',
    height: 200,
    color: 'red',
  },

  otp11: {
    flex: 1,
    alignItems: 'center',
    marginVertical: scale(15),
  },

  otp12: {
    paddingHorizontal: scale(50),
    backgroundColor: '#2D8989',
    borderRadius: scale(50),
  },

  otp13: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },

  otp14: {
    color: '#2D8989',
    fontSize: scale(15),
    fontWeight: 'bold',
  },
});

export default styles;
