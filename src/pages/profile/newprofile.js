import {
  React,
  useEffect,
  connect,
  View,
  Image,
  EvaIcon,
  CommonActions,
} from 'libraries';
import { Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import ActionButton from 'react-native-action-button';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-eva-icons';
import { ListItem } from 'react-native-elements';
import {
  GetProfileAction,
  LogoutAction,
  UpdateImageAction,
} from '../../configs/redux/reducers/profile/profileActions';
import baseUrl from '../../configs/api/url';
import { sendEmail } from 'configs/redux/reducers/auth/authActions';
import { LogBox, StatusBar } from 'react-native';

const NewProfilePage = (props) => {
  const {
    navigation,
    myCoursesCount,
    myCompletedCourseCount,
    myOwnCoursesCount,
    mySubsCourseCount,
    fullname,
    email,
    profilePicture,
    isEmailVerified,
    dispatchGetProfile,
    dispatchLogout,
    dispatchSendEmail,
  } = props;

  LogBox.ignoreAllLogs();
  useEffect(() => {
    dispatchGetProfile();
  }, [dispatchGetProfile]);

  useEffect(() => {
    console.log(baseUrl.profil.getprofilepicture + profilePicture);
  }, [profilePicture]);

  const navigationReset = (page) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: page }],
      }),
    );
  };

  const doLogout = () => {
    dispatchLogout();
    navigationReset('LoginPage');
  };

  function renderVerified() {
    if (isEmailVerified == null) {
      return <View />;
    } else if (isEmailVerified) {
      return (
        <Icon
          name="checkmark-circle-2"
          width={scale(20)}
          height={scale(20)}
          fill={'orange'}
          style={{ marginLeft: scale(10) }}
        />
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            dispatchSendEmail();
            navigation.navigate('VerificationPage');
          }}>
          <View style={styles.style1}>
            <Text style={styles.style2}>Not Verified</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  return (
    <View style={[styles.container, { marginTop: StatusBar.currentHeight }]}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />

      <View
        style={{
          flex: 1.5,
          backgroundColor: Color.primaryColor,
          // borderRadius: scale(30),
          borderBottomLeftRadius: scale(0),
          borderBottomRightRadius: scale(0),
          paddingBottom: scale(15),
          // marginHorizontal: scale(5),
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: scale(15),
            paddingTop: scale(10),
          }}>
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'left',
              fontSize: scale(30),
            }}>
            Profile
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('EditPage');
            }}>
            <Icon
              name="edit"
              width={scale(30)}
              height={scale(30)}
              fill={'white'}
              style={styles.style8}
            />
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View
            style={{
              width: scale(120),
              height: scale(120),
              position: 'relative',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              // source={require('../../assets/images/foto.jpg')}
              source={{
                uri:
                  profilePicture == null
                    ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png'
                    : baseUrl.profil.getprofilepicture + profilePicture,
              }}
              style={styles.avatarProfile}
            />

            <View
              style={{
                position: 'absolute',
                bottom: scale(10),
                right: scale(10),
                backgroundColor: Color.backgroundColor,
                // padding: scale(10),
                borderRadius: scale(50),
                height: scale(40),
                width: scale(40),
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('AvatarPage')}>
                <EvaIcon
                  name={'camera-outline'}
                  height={scale(20)}
                  width={scale(20)}
                  fill={Color.primaryColor}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Text style={styles.style15}>{fullname}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.style17}>{email}</Text>
              {renderVerified()}
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: scale(10),
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('PreferredCategoryPage');
                }}>
                <View style={styles.style19}>
                  <Icon
                    name="settings-outline"
                    width={scale(30)}
                    height={scale(30)}
                    fill={'white'}
                    style={styles.style20}
                  />
                  <Text style={styles.style21}>Preference</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {/* <View>
          <View style={styles.style10}>
            <View style={styles.style11}>
              <Image
                // source={require('../../assets/images/foto.jpg')}
                source={{
                  uri:
                    profilePicture == null
                      ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png'
                      : baseUrl.profil.getprofilepicture + profilePicture,
                }}
                style={styles.style12}
              />
              <ActionButton
                buttonColor="#2D8989"
                offsetY={30}
                offsetX={30}
                size={40}
                onPress={() => {
                  navigation.navigate('AvatarPage');
                }}
              />
            </View>
            <View style={styles.style13}>
              <View style={styles.style14}>
                <View style={{ marginBottom: scale(10) }}>
                  <Text style={styles.style15}>{fullname}</Text>

                  <View style={styles.style16}>
                    <Text style={styles.style17}>{email}</Text>
                    {renderVerified()}
                  </View>
                </View>
              </View>

              <View style={styles.style18}>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('PreferredCategoryPage');
                  }}>
                  <View style={styles.style19}>
                    <Icon
                      name="settings-outline"
                      width={scale(30)}
                      height={scale(30)}
                      fill={'white'}
                      style={styles.style20}
                    />
                    <Text style={styles.style21}>Preference</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View> */}
      </View>

      <View style={styles.style22}>
        <View style={styles.style23}>
          <Text style={styles.style24}>My Dashboard</Text>
        </View>
        <View style={styles.style25}>
          <View style={styles.style26}>
            <View style={styles.style48}>
              <Image
                source={require('../../assets/images/book.png')}
                style={styles.style28}
              />
            </View>
            <View style={styles.style29}>
              <Text style={styles.style30}> My Course</Text>
            </View>

            <View style={styles.style31}>
              <Text style={styles.style32}> {myCoursesCount}</Text>
            </View>
          </View>

          <View style={styles.style33}>
            <View style={styles.style48}>
              <Image
                source={require('../../assets/images/book1.png')}
                style={styles.style28}
              />
            </View>
            <View style={styles.style36}>
              <Text style={styles.style37}> My Complete Course</Text>
            </View>

            <View style={styles.style38}>
              <Text style={styles.style39}> {myCompletedCourseCount}</Text>
            </View>
          </View>

          <View style={styles.style40}>
            <View style={styles.style48}>
              <Image
                source={require('../../assets/images/book2.png')}
                style={styles.style28}
              />
            </View>
            <View style={styles.style43}>
              <Text style={styles.style44}> My Own Courses</Text>
            </View>

            <View style={styles.style45}>
              <Text style={styles.style46}> {myOwnCoursesCount}</Text>
            </View>
          </View>

          <View style={styles.style47}>
            <View style={styles.style48}>
              <Image
                source={require('../../assets/images/book4.png')}
                style={styles.style49}
              />
            </View>
            <View style={styles.style50}>
              <Text style={styles.style51}> My Subs Courses</Text>
            </View>

            <View style={styles.style52}>
              <Text style={styles.style53}> {mySubsCourseCount}</Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.style54}>
        <TouchableOpacity onPress={() => doLogout()}>
          <View style={{ marginTop: scale(10) }}>
            <ListItem
              containerStyle={{
                borderRadius: scale(25),
                marginLeft: scale(20),
                marginRight: scale(20),
              }}>
              <Icon
                name="log-out-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
                style={styles.style55}
              />
              <ListItem.Content>
                <ListItem.Title style={styles.style56}>Logout</ListItem.Title>
              </ListItem.Content>
              <Icon
                name="arrow-ios-forward-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
              />
            </ListItem>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    myCoursesCount: state.profileStore.myCoursesCount,
    myCompletedCourseCount: state.profileStore.myCompletedCourseCount,
    myOwnCoursesCount: state.profileStore.myOwnCoursesCount,
    mySubsCourseCount: state.profileStore.mySubsCourseCount,
    fullname: state.profileStore.fullname,
    email: state.profileStore.email,
    profilePicture: state.profileStore.profilePicture,
    isEmailVerified: state.authStore.isEmailVerified,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetProfile: () => dispatch(GetProfileAction()),
    dispatchUpdateImage: (payload) => dispatch(UpdateImageAction(payload)),
    dispatchSendEmail: () => dispatch(sendEmail()),
    dispatchLogout: () => dispatch(LogoutAction()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProfilePage);
