import { BottomTabBar } from '@react-navigation/bottom-tabs';
import { StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  // textInput: {
  //   marginHorizontal:scale(50),
  //   borderBottomWidth:2,
  //   borderColor: Color.primaryColor,
  //   color:Color.primaryColor,
  //   marginTop:scale(10),
  //   flexDirection:'row'
  // },
  textInputContainer: {
    flexDirection: 'row',
    marginHorizontal: scale(50),
    borderRadius: scale(50),
    marginTop: scale(15),
    backgroundColor: 'white',
    color: Color.primaryColor,
    alignItems: 'center',
    paddingVertical: scale(5),
  },
  imageStyle: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },
});

export default styles;
