import {
  React,
  useEffect,
  useState,
  connect,
  View,
  TextInput,
  Image,
  FontAwesome5,
  TouchableOpacity,
  StatusBar,
  ScrollView,
} from 'libraries';
import { Text, Button } from 'components';
import IMG from 'assets/images';
import styles from './style';
import { appString, METRICS, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import { Color } from '../../utils/styles/colors';
import LottieView from 'lottie-react-native';
import { RegisterAction } from 'configs/redux/reducers/auth/authActions';

const RegisterPage = (props) => {
  const {
    navigation,
    dispatchRegister,
    errorRegister,
    isLoadingRegister,
  } = props;

  const [username, setUserName] = useState('');
  const [fullname, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isValid, setIsValid] = useState({
    username: true,
    fullname: true,
    email: true,
    password: true,
  });

  useEffect(() => {
    // isi initial first load
    //contoh update state global via redux
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    if (errorRegister) {
      console.log(errorRegister.status);
      if (errorRegister.status == 422) {
        alert(errorRegister.data.error.message);
      }
    }
  }, [errorRegister]);

  const submitRegister = () => {
    const check = validation('all');
    console.log(isValid);
  };

  const validation = (item) => {
    const isUserName = /^[a-z0-9]+$/i.test(username);
    const isFullname = /^[a-z\d\-_\s]+$/i.test(fullname);
    const isEmail = /^([A-Za-z][A-Za-z0-9\-\.\_]*)\@([A-Za-z][A-Za-z0-9\-\_]*)(\.[A-Za-z][A-Za-z0-9\-\_]*)+$/i.test(
      email,
    );
    const isPassword = /^\S*$/.test(password);

    if (item == 'all') {
      setIsValid({
        ...isValid,
        username: isUserName,
        fullname: isFullname,
        email: isEmail,
        password: isPassword,
      });
      console.log(`ini dta ${username} ${fullname} ${email} ${password}`);
      if (isUserName && isFullname && isPassword && isEmail) {
        console.log({
          body: {
            username,
            fullname,
            password,
            email,
          },
        });
        dispatchRegister({
          body: {
            username,
            fullname,
            password,
            email,
          },
        });
      }
    } else if (item == 'username') {
      setIsValid({
        ...isValid,
        username: isUserName,
      });
    } else if (item == 'fullname') {
      setIsValid({
        ...isValid,
        fullname: isFullname,
      });
    } else if (item == 'email') {
      setIsValid({
        ...isValid,
        email: isEmail,
      });
    } else {
      setIsValid({
        ...isValid,
        password: isPassword,
      });
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <ScrollView>
        <Text
          bold
          style={{
            fontSize: scale(50),
            textAlign: 'center',
            marginTop: scale(20),
            color: Color.primaryColor,
          }}>
          Sign Up
        </Text>
        <Text
          style={{
            textAlign: 'center',
            marginTop: scale(5),
            color: Color.darkGray,
            fontSize: scale(20),
          }}>
          Fill the details & create your account
        </Text>
        <View style={{ alignItems: 'center' }}>
          <LottieView
            style={{ height: scale(180) }}
            source={require('../../assets/animasi/signup.json')}
            autoPlay
            loop
          />
        </View>
        <View style={{ marginTop: scale(10) }}>
          <View style={[styles.textInputContainer]}>
            <Image style={styles.imageStyle} source={IMG.person} />
            <TextInput
              style={{ width: '100%' }}
              pattern={'/^[a-z0-9]+$/i'}
              onValidation={(check) =>
                setIsValid({ ...isValid, username: check })
              }
              placeholder={'Username'}
              onChangeText={(text) => {
                setUserName(text);
                validation('username');
              }}
              color={Color.primaryColor}
            />
          </View>
          {!isValid.username ? (
            <Text style={{ color: 'red', marginLeft: scale(65) }}>
              accept only alpha numeric
            </Text>
          ) : (
            <View />
          )}
          <View
            style={[
              styles.textInputContainer,
              { borderColor: !isValid.fullname ? 'red' : Color.primaryColor },
            ]}>
            <Image style={styles.imageStyle} source={IMG.person} />
            <TextInput
              style={{ width: '100%' }}
              placeholder={'Fullname'}
              onChangeText={(text) => {
                setFullName(text);
                validation('fullname');
              }}
              color={Color.primaryColor}
            />
          </View>
          {!isValid.fullname ? (
            <Text style={{ color: 'red', marginLeft: scale(65) }}>
              accept only alpha space
            </Text>
          ) : (
            <View />
          )}
          <View
            style={[
              styles.textInputContainer,
              { borderColor: !isValid.email ? 'red' : Color.primaryColor },
            ]}>
            <Image style={styles.imageStyle} source={IMG.mail} />
            <TextInput
              style={{ width: '100%' }}
              placeholder={'Email'}
              onChangeText={(text) => {
                setEmail(text);
                validation('email');
              }}
              color={Color.primaryColor}
            />
          </View>
          {!isValid.email ? (
            <Text style={{ color: 'red', marginLeft: scale(65) }}>
              accept only email type
            </Text>
          ) : (
            <View />
          )}
          <View
            style={[
              styles.textInputContainer,
              { borderColor: !isValid.password ? 'red' : Color.primaryColor },
            ]}>
            <Image style={styles.imageStyle} source={IMG.lock} />
            <TextInput
              style={{ width: '100%' }}
              placeholder={'Password'}
              onChangeText={(text) => {
                setPassword(text);
                validation('password');
              }}
              color={Color.primaryColor}
              secureTextEntry
            />

            <TouchableOpacity style={{ marginLeft: scale(130) }}>
              <Image style={styles.imageStyle} source={IMG.eye} />
            </TouchableOpacity>
          </View>
          {!isValid.password ? (
            <Text style={{ color: 'red', marginLeft: scale(65) }}>
              accept only non whitespace
            </Text>
          ) : (
            <View />
          )}
        </View>
        <Button
          title="Sign Up"
          onPress={() => {
            submitRegister();
          }}
          isLoading={isLoadingRegister}
          buttonStyle={{
            marginTop: scale(15),
            width: METRICS.window.width * 0.75,
            alignSelf: 'center',
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: scale(10),
          }}>
          <Text style={{ color: Color.darkGray }}>
            Already have an account?
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('LoginPage');
            }}>
            <Text style={{ fontWeight: 'bold', color: Color.dimGray }}>
              {' '}
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    errorRegister: state.authStore.errorRegister,
    isLoadingRegister: state.authStore.isLoadingRegister,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchRegister: (payload) => dispatch(RegisterAction(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
