import { StyleSheet, StatusBar } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    backgroundColor: Color.backgroundColor,
    paddingHorizontal: scale(15),
  },
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalContent: {
    backgroundColor: 'white',
    borderRadius: scale(10),
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    position: 'relative',
    maxHeight: scale(250),
    width: METRICS.window.width * 0.8,
  },
  imageContent: {
    width: METRICS.window.width * 0.8,
    height: scale(120),
    resizeMode: 'stretch',
    position: 'absolute',
    top: 0,
  },
  buttonGiveReview: {
    backgroundColor: Color.primaryColor,
    paddingVertical: scale(10),
    borderRadius: scale(20),
    marginVertical: scale(20),
  },
  textReviewButton: { color: 'white', fontSize: scale(14) },
  topBar: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: scale(10),
    marginBottom: scale(20),
  },
  backButton: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(50),
    padding: scale(4),
  },
  titleExamPage: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
  cardQuestion: {
    backgroundColor: 'white',
    borderRadius: scale(10),
    marginBottom: scale(10),
    paddingHorizontal: scale(10),
    paddingVertical: scale(5),
  },
  subItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: scale(30),
    paddingVertical: scale(10),
  },
  radioButton: {
    height: scale(20),
    width: scale(20),
    borderRadius: scale(20),
    backgroundColor: Color.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  multiSelectButton: {
    height: scale(20),
    width: scale(20),
    backgroundColor: Color.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subItemText: { flex: 1, marginLeft: scale(5) },
  noSelect: {
    height: scale(20),
    width: scale(20),
    borderRadius: scale(20),
    backgroundColor: '#EDF1F7',
  },
  noSelectMultiple: {
    height: scale(20),
    width: scale(20),
    backgroundColor: '#EDF1F7',
  },
  scoreWrapper: { alignItems: 'center' },
  score: (value) => {
    return {
      color:
        value <= 50 ? 'red' : value > 50 && value <= 75 ? 'orange' : 'green',
      fontSize: scale(40),
    };
  },
  questionWrrapper: { flex: 1 },
});

export default styles;
