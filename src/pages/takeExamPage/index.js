import {
  React,
  useEffect,
  useState,
  connect,
  View,
  EvaIcon,
  Modal,
  Image,
} from 'libraries';
import { Button, Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import { Pressable, ScrollView, StatusBar } from 'react-native';
import { getExam } from 'configs/redux/reducers/course/courseActions';
import API from 'configs/api';
import IMG from 'assets/images';

const TakeExamPage = (props) => {
  const { navigation, examQuestion, dispatchGetExam, route } = props;
  const { data } = route.params;
  const [finalAnswer, setFinalAnswer] = useState({});
  const [visibileReview, setVisibleReview] = useState(false);
  const [examResult, setExamResult] = useState({
    message: 'init',
  });
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    dispatchGetExam({
      paramsId: data.id,
    });
  }, []);

  useEffect(() => {
    const newData = {};
    examQuestion.data.forEach((item, index) => {
      newData[item.questionId] = {
        questionId: item.questionId,
        answer: [],
      };
    });
    setFinalAnswer(newData);
  }, [examQuestion]);

  const submitExam = async (payloadExam) => {
    try {
      setIsLoading(true);
      const res = await API.takeExam({
        body: payloadExam,
      });
      if (res) {
        setIsLoading(false);
        setExamResult(res);
        setVisibleReview(true);
      }
    } catch (e) {
      console.log(e);
    }
  };

  function removeItemAll(arr, value) {
    var i = 0;
    while (i < arr.length) {
      if (arr[i] === value) {
        arr.splice(i, 1);
      } else {
        ++i;
      }
    }
    return arr;
  }

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <View style={styles.topBar}>
        <Pressable
          onPress={() => {
            navigation.pop();
          }}>
          <View style={styles.backButton}>
            <EvaIcon
              name={'arrow-ios-back-outline'}
              width={scale(20)}
              height={scale(20)}
              fill={'white'}
            />
          </View>
        </Pressable>

        <Text bold style={styles.titleExamPage}>
          Take Exam
        </Text>
      </View>
      <View style={styles.questionWrrapper}>
        {examQuestion.data.length > 0 ? (
          <ScrollView>
            {examQuestion.data.map((item, index) => {
              return (
                <View key={index} style={styles.cardQuestion}>
                  <Text bold style={{ fontSize: scale(20) }}>
                    Question{` ${index + 1}`}: {` ${item.question}`}
                  </Text>
                  <Text style={{ fontSize: scale(15) }}>
                    Type:{` ${item.type}`}
                  </Text>
                  {item.choices.map((subitem, subindex) => {
                    let isExist = false;
                    if (finalAnswer[item.questionId]) {
                      const checked = finalAnswer[item.questionId];
                      isExist = checked.answer.includes(subitem.key);
                    }

                    return (
                      <Pressable
                        key={subindex}
                        onPress={() => {
                          let newData = {
                            ...finalAnswer[item.questionId],
                          };
                          if (item.type === 'single') {
                            newData.answer = [];
                            newData.answer.push(subitem.key);
                            let finalDataN = {
                              ...finalAnswer,
                            };
                            finalDataN[item.questionId] = newData;

                            setFinalAnswer(finalDataN);
                          } else {
                            if (newData.answer.includes(subitem.key)) {
                              newData.answer = removeItemAll(
                                newData.answer,
                                subitem.key,
                              );
                            } else {
                              newData.answer.push(subitem.key);
                            }
                            let finalDataN = {
                              ...finalAnswer,
                            };
                            finalDataN[item.questionId] = newData;

                            setFinalAnswer(finalDataN);
                          }
                          if (
                            item.type === 'single' &&
                            newData.answer.includes(subitem.key)
                          ) {
                            isExist = true;
                            newData.answer = [subitem.key];
                          } else {
                            isExist = false;
                          }
                          console.log(newData.answer);
                        }}>
                        <View style={styles.subItemRow}>
                          <Text style={styles.subItemText}>
                            {subitem.value}
                          </Text>
                          {isExist ? (
                            <View
                              style={
                                item.type === 'single'
                                  ? styles.radioButton
                                  : styles.multiSelectButton
                              }>
                              <EvaIcon
                                name="checkmark-outline"
                                width={scale(10)}
                                height={scale(10)}
                                fill={'white'}
                              />
                            </View>
                          ) : (
                            <View
                              style={
                                item.type === 'single'
                                  ? styles.noSelect
                                  : styles.noSelectMultiple
                              }
                            />
                          )}
                        </View>
                      </Pressable>
                    );
                  })}
                </View>
              );
            })}
          </ScrollView>
        ) : (
          <View />
        )}
      </View>
      <Button
        title="Submit Exam"
        onPress={() => {
          const questions = [];
          Object.keys(finalAnswer).forEach((item, index) => {
            questions.push(finalAnswer[item]);
          });
          const payloadforExam = {
            courseId: data.id,
            questions,
          };
          submitExam(payloadforExam);
        }}
        isLoading={isLoading}
      />
      <Modal visible={visibileReview} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Image source={IMG.congratulation} style={styles.imageContent} />
            <Text style={{ marginTop: scale(60) }}>CONGRATULATIONS</Text>
            <Text bold style={{ marginTop: scale(10) }}>
              You have successfully complete your course
            </Text>
            {examResult.message !== 'init' ? (
              <View style={styles.scoreWrapper}>
                <Text>Your Score</Text>
                <Text style={{}}>
                  <Text bold style={styles.score(examResult.data.mark)}>
                    {examResult.data.mark}
                  </Text>
                  <Text>/100</Text>
                </Text>
              </View>
            ) : (
              <View />
            )}
            <Button
              title="Give Review"
              buttonStyle={styles.buttonGiveReview}
              textStyle={styles.textReviewButton}
              onPress={() => {
                setVisibleReview(false);
                navigation.navigate('ReviewPage', {
                  data,
                });
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    examQuestion: state.courseStore.examQuestion,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetExam: (payload) => dispatch(getExam(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TakeExamPage);
