import {
  MyCourseTabBar,
  MyCourseTabView,
  MySubscriptionTabView,
} from 'components';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import {
  connect,
  Dimensions,
  React,
  SafeAreaView,
  StatusBar,
  useState,
} from 'libraries';
import { TabView } from 'react-native-tab-view';
import { Color } from 'utils';
import styles from './style';

const MyCoursePage = (props) => {
  const [tabIndex, setTabIndex] = useState(0);
  const [routes] = useState([
    { key: 'subscriptions', title: 'Subscriptions' },
    { key: 'course', title: 'My Courses' },
  ]);
  const initialLayout = { width: Dimensions.get('window').width };
  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'subscriptions':
        return <MySubscriptionTabView {...props} />;
      case 'course':
        return <MyCourseTabView {...props} />;
      default:
        return null;
    }
  };

  return (
    <SafeAreaView
      style={[styles.container, { paddingTop: StatusBar.currentHeight }]}>
      <StatusBar
        translucent
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <TabView
        // renderPager={(tabProps) => <ScrollPager {...tabProps} />}
        navigationState={{ index: tabIndex, routes }}
        renderTabBar={MyCourseTabBar}
        renderScene={renderScene}
        lazy={true}
        lazyPreloadDistance={1}
        onIndexChange={setTabIndex}
        initialLayout={initialLayout}
      />
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchExample: () => dispatch(exampleAction()),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCoursePage);
