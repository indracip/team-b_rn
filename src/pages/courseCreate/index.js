import { BackButton, Button, CategorySelect, Divider, Text } from 'components';
import { getCourseCategory } from 'configs/redux/reducers/course/courseActions';
import {
  postCourse,
  setSelectedCategory,
} from 'configs/redux/reducers/myCourse/myCourseActions';
import {
  Animated,
  BottomSheet,
  connect,
  KeyboardAvoidingView,
  Platform,
  React,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  useEffect,
  useRef,
  useState,
  View,
} from 'libraries';
import {
  Image,
  Keyboard,
  ScrollView,
  StatusBar,
  TouchableWithoutFeedback,
} from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import { verticalScale } from 'utils';
import styles from './style';

const CourseCreatePage = (props) => {
  const {
    navigation,
    selectedCatgory,
    dispatchSetSelectedCategories,
    dispatchPostCourse,
    isLoading,
    error,
  } = props;
  const sheetRef = useRef(null);
  const fall = new Animated.Value(1);
  const [courseName, setCourseName] = useState('');
  const [courseDetail, setCourseDetail] = useState('');
  const [courseImage, setCourseImage] = useState(null);
  const [disable, setDisable] = useState(false);

  const handleOutsidePress = () => {
    // Keyboard.dismiss();
    sheetRef.current.snapTo(2);
  };

  const handleSheetOpen = () => {
    // setShadowPointerEvents(true);
    // console.log(sheetRef.current);
    setDisable(true);
  };

  const handleSheetClose = () => {
    // setShadowPointerEvents(false);
    setDisable(false);
  };

  const handleSelectImage = () => {
    const options = {
      mediaType: 'photo',
      quality: 1,
    };

    launchImageLibrary(options, (response) => {
      console.log(response);

      // Same code as in above section!
      const selectedImage = {
        uri: response.uri,
        type: response.type, // mime type
        name: response.fileName,
        size: response.fileSize,
      };

      setCourseImage(selectedImage);
    });
  };

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const renderContent = () => (
    <View style={styles.bottomSheetContent}>
      <CategorySelect sheetRef={sheetRef} />
    </View>
  );

  const renderShadow = () => {
    const animatedShadowOpacity = Animated.interpolate(fall, {
      inputRange: [0, 1],
      outputRange: [0.5, 0],
    });

    return (
      <Animated.View
        pointerEvents={'none'}
        style={[
          styles.shadowContainer,
          {
            opacity: animatedShadowOpacity,
          },
        ]}
      />
    );
  };

  const postCreateCorse = async () => {
    try {
      const form = new FormData();
      form.append('coursePicture', courseImage);
      form.append('categoryId', parseInt(selectedCatgory.id, 10));
      form.append('title', courseName);
      form.append('desc', courseDetail);

      const payload = {
        body: form,
        type: 'form-data',
      };
      const response = await dispatchPostCourse(payload);
      navigation.replace('CourseEditPage', { data: response });
    } catch (error) {}
  };

  useEffect(() => {
    return () => {
      dispatchSetSelectedCategories('');
    };
  }, []);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <StatusBar translucent={false} />
      <BottomSheet
        ref={sheetRef}
        initialSnap={2}
        snapPoints={[verticalScale(600), verticalScale(300), 0]}
        renderHeader={renderHeader}
        renderContent={renderContent}
        callbackNode={fall}
        onOpenStart={handleSheetOpen}
        onCloseEnd={handleSheetClose}
        enabledContentTapInteraction={false}
      />
      <TouchableWithoutFeedback onPress={handleOutsidePress}>
        <SafeAreaView style={styles.container}>
          <View style={styles.contentHeader}>
            <BackButton
              disabled={disable}
              onPress={() => {
                navigation.goBack();
              }}
            />
            <Text bold style={styles.headerTitle}>
              Create Course
            </Text>
          </View>

          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.scrollViewContainer}>
            <View style={[styles.createForm]}>
              <Text style={styles.inputLabel}>Course Category</Text>
              <TouchableOpacity
                onPress={() => {
                  sheetRef.current.snapTo(1);
                  Keyboard.dismiss();
                }}>
                <View pointerEvents="none" style={styles.textCategoryContainer}>
                  <TextInput
                    style={styles.textCategory}
                    value={selectedCatgory.name}
                    pointerEvents="none"
                  />
                </View>
              </TouchableOpacity>
              <Divider />
              <Text style={styles.inputLabel}>Course Name</Text>
              <TextInput
                onFocus={() => {
                  handleOutsidePress();
                }}
                style={styles.inputCourseName}
                value={courseName}
                onChangeText={(value) => setCourseName(value)}
              />
              <Divider />
              <Text style={styles.inputLabel}>Description</Text>
              <TextInput
                onFocus={() => {
                  handleOutsidePress();
                }}
                numberOfLines={15}
                textAlignVertical="top"
                multiline={true}
                style={styles.inputDecription}
                value={courseDetail}
                onChangeText={(value) => setCourseDetail(value)}
              />
              <Divider />
              <Text style={styles.inputLabel}>Course Image</Text>
              {courseImage && (
                <View style={styles.coourseImageContainer}>
                  <Image
                    source={{ uri: courseImage.uri }}
                    style={styles.courseImage}
                  />
                </View>
              )}
              <Button
                title={'Upload Image'}
                buttonStyle={styles.nextButton}
                textStyle={styles.nextBtnLabel}
                onPress={() => handleSelectImage()}
              />
              <View />
              <Button
                title={'Create'}
                buttonStyle={styles.nextButton}
                textStyle={styles.nextBtnLabel}
                isLoading={isLoading}
                onPress={() => postCreateCorse()}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      </TouchableWithoutFeedback>
      {renderShadow()}
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseCategories: state.courseStore.course_category,
    selectedCatgory: state.myCourseStore.selectedCatgory,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetCourseCatgories: () => dispatch(getCourseCategory()),
    dispatchSetSelectedCategories: (payload) =>
      dispatch(setSelectedCategory(payload)),
    dispatchPostCourse: (payload) => dispatch(postCourse(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseCreatePage);
