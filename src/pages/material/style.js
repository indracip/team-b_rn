import { StatusBar } from 'react-native';
import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  header: {
    marginTop: StatusBar.currentHeight,
    marginBottom: scale(10),
  },
  title: {
    backgroundColor: Color.primaryColor,
    padding: scale(10),
  },
  titleText: {
    fontSize: scale(16),
    fontWeight: 'bold',
    color: 'white',
  },
  content: {
    flex: 1,
    paddingVertical: scale(10),
  },
  contentContainer: {
    flexGrow: 1,
    paddingBottom: scale(100),
    // backgroundColor: 'blue',
  },
  contentBody: {
    flex: 1,
  },
  // material
  curriculumCards: {
    // flex: 1,
  },
  bottomActionContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    padding: scale(5),
    borderRadius: scale(5),
    backgroundColor: Color.lightGray,
  },
  btnCreate: {
    backgroundColor: Color.primaryColor,
  },
  btnCreateLabel: {
    color: 'white',
    fontSize: scale(14),
    fontWeight: '400',
  },
  emptyMaterials: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyMaterialsLabel: {
    fontSize: scale(16),
    fontWeight: '400',
  },
});

export default styles;
