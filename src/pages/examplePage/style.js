import {StyleSheet} from 'react-native';
import {Color, scale} from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default styles;
