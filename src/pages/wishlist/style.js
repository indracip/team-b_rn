import { StatusBar } from 'react-native';
import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const TemplateStyles = {
  defaultShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  container: {
    marginTop: StatusBar.currentHeight,
    flex: 1,
    backgroundColor: Color.backgroundColor,
    paddingTop: scale(10),
    paddingHorizontal: scale(10),
  },
  titleContainer: {
    alignItems: 'baseline',
  },
  pageTitle: {
    // backgroundColor: 'yellow',
  },
  titleText: {
    fontSize: scale(24),
    fontWeight: 'bold',
  },
  titleUnderline: {
    height: 6,
    backgroundColor: '#E9B5D6',
    borderRadius: 8,
  },
  wishlistContainer: {
    flex: 1,
    paddingTop: scale(10),
  },
  emptyWishlist: {
    flex: 1,
    // backgroundColor: 'yellow',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyWishlistText1: {
    fontSize: scale(24),
    fontWeight: 'bold',
  },
  emptyWishlistText2: {
    fontSize: scale(18),
    color: 'gray',
  },
  wishlistCardContainer: {
    flex: 1,
    paddingTop: scale(5),
    paddingHorizontal: scale(5),
  },
  wishlistCard: {
    flex: 1,
    backgroundColor: 'white',
    ...TemplateStyles.defaultShadow,
    width: scale(155),
    borderRadius: scale(15),
    marginBottom: scale(15),
    // overflow: 'hidden',
  },
  wishlistImageContainer: {
    height: scale(75),
    borderTopStartRadius: scale(15),
    borderTopEndRadius: scale(15),
    overflow: 'hidden',
  },
  wishlistImage: {
    width: '100%',
    height: scale(100),
    ...StyleSheet.absoluteFillObject,
  },
  wishlistContent: {
    flex: 1,
    backgroundColor: 'white',
    padding: scale(10),
    borderBottomStartRadius: scale(15),
    borderBottomEndRadius: scale(15),
    minHeight: scale(90),
  },
  wishlistName: {
    fontSize: scale(16),
    fontWeight: 'bold',
  },
  wishlistTrainer: {
    color: 'gray',
  },
  wishlistCardAddMore: {
    flex: 1,
    backgroundColor: Color.primaryColor,
    ...TemplateStyles.defaultShadow,
    width: scale(155),
    borderRadius: scale(15),
    marginBottom: scale(15),
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSearch: {
    backgroundColor: Color.primaryColor,
    paddingVertical: scale(6),
    paddingHorizontal: scale(28),
    borderRadius: 100,
    marginTop: scale(10),
    ...TemplateStyles.defaultShadow,
  },
  btnSearchText: {
    color: 'white',
    fontSize: scale(18),
  },
});

export default styles;
