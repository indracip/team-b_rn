/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
// @ts-nocheck
import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Image,
  Icon,
  EvaIcon,
  useFocusEffect,
  useIsFocused,
  StatusBar,
} from 'libraries';
import { Button, Text } from 'components';
import styles from './style';
import { appString, Color, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import EmptyImage from '../../assets/images/emptyWishlist.svg';
import { fetchMyWishlist } from 'configs/redux/reducers/wishlist/wishlistAction';
import WishlistCard from 'components/molecules/wishlistCard';

const dummyApiMyWishlist = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      {
        id: 8,
        userId: '5d107f96-8709-52cf-92a0-a10d26bde2b2',
        courseId: 1,
        createdAt: '2021-02-04T12:50:25.257Z',
        updatedAt: '2021-02-04T12:50:25.257Z',
        deletedAt: null,
        courseDetails: {
          id: 1,
          authorId: '5ff6553e-1ff8-55f6-a7d4-04528fbf5048',
          categoryId: 1,
          title: 'Materi Penting',
          desc: 'Materi Penting',
          picture: 'materipenting.jpg',
          isPublished: false,
          createdAt: '2021-01-31T18:26:51.000Z',
          updatedAt: '2021-02-04T17:43:22.041Z',
          deletedAt: null,
        },
      },
      {
        id: 3,
        userId: '5d107f96-8709-52cf-92a0-a10d26bde2b2',
        courseId: 8,
        createdAt: '2021-02-04T12:28:09.646Z',
        updatedAt: '2021-02-04T12:28:09.646Z',
        deletedAt: null,
        courseDetails: {
          id: 8,
          authorId: 'e47c1727-6b6a-5c05-b6da-1b62cd8673ca',
          categoryId: 2,
          title: 'Belajar Forex',
          desc: 'Cara belajar investasi forex dengan baik dan benar',
          picture: 'materipenting.jpg',
          isPublished: true,
          createdAt: '2021-02-04T02:22:58.000Z',
          updatedAt: '2021-02-04T02:22:58.556Z',
          deletedAt: null,
        },
      },
      {
        id: 2,
        userId: '5d107f96-8709-52cf-92a0-a10d26bde2b2',
        courseId: 9,
        createdAt: '2021-02-04T12:25:00.609Z',
        updatedAt: '2021-02-04T12:25:00.609Z',
        deletedAt: null,
        courseDetails: {
          id: 9,
          authorId: 'e47c1727-6b6a-5c05-b6da-1b62cd8673ca',
          categoryId: 2,
          title: 'Belajar Saham',
          desc:
            'Investasikan uangmu pada saham sekarang juga, jangan ketinggalan!',
          picture: 'materipenting.jpg',
          isPublished: true,
          createdAt: '2021-02-02T02:23:40.000Z',
          updatedAt: '2021-02-04T02:23:40.208Z',
          deletedAt: null,
        },
      },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};

const WishlistPage = (props) => {
  const {
    navigation,
    myWishlist,
    dispatchGetMyWishlist,
    isLoading,
    error,
  } = props;
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatchGetMyWishlist();
  }, []);

  useEffect(() => {
    if (isFocused) {
      console.log('fetch data');
      dispatchGetMyWishlist();
    }
  }, [isFocused]);

  return (
    <SafeAreaView style={styles.safeArea}>
      <StatusBar
        translucent
        barStyle="dark-content"
        backgroundColor={Color.backgroundColor}
      />
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <View style={styles.pageTitle}>
            <Text style={styles.titleText}>Wishlist</Text>
            <View style={styles.titleUnderline} />
          </View>
        </View>
        <View style={styles.wishlistContainer}>
          <FlatList
            numColumns={2}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ flexGrow: 1 }}
            columnWrapperStyle={{
              flex: 1,
              justifyContent: 'space-between',
              paddingBottom: scale(20),
            }}
            data={
              myWishlist && myWishlist.length > 0
                ? [...myWishlist, { add: 'more' }]
                : myWishlist
            }
            keyExtractor={(item) => String(item.id)}
            ListEmptyComponent={() => {
              return (
                <View style={styles.emptyWishlist}>
                  <EmptyImage width={scale(250)} height={scale(250)} />
                  <Text style={styles.emptyWishlistText1}>
                    It's pretty lonely here,
                  </Text>
                  <Text style={styles.emptyWishlistText1}>
                    don't you think?
                  </Text>
                  <Text style={styles.emptyWishlistText2}>
                    Search your first course
                  </Text>
                  <Text style={styles.emptyWishlistText2}>
                    and save it to your wishlist
                  </Text>
                  <TouchableOpacity
                    style={styles.btnSearch}
                    onPress={() => {
                      navigation.navigate('SearchPage');
                    }}>
                    <Text style={styles.btnSearchText}>Search</Text>
                  </TouchableOpacity>
                </View>
              );
            }}
            renderItem={({ item, index }) => {
              return item.add == undefined ? (
                <WishlistCard {...props} item={item} index={index} />
              ) : (
                <WishlistCard {...props} add={item.add} />
              );
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    myWishlist: state.wishlistStore.wishlist_data,
    isLoading: state.searchStore.isLoading,
    error: state.searchStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetMyWishlist: () => dispatch(fetchMyWishlist()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WishlistPage);
